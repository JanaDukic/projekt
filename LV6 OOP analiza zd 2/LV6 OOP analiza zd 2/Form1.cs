﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV6_OOP_analiza_zd_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
       
        List<string> ListaPojmova = new List<string>();
        string path = "C:\\Users\\Korisnik\\Desktop\\lv6zd2.txt";
        string NovoGenenriraniPojam, skriveniString,rez;
        Random r = new Random();
        int index, brojPokusaja = 6,br; //glava, trup, 2 ruke, 2 noge
        char SLOVO;
        char[] privremeniRez, skrivenaslova, odvojenaslova;

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pokazi_pojam_Click(object sender, EventArgs e)
        {
            tb_rjesenje_vjesala.Text = NovoGenenriraniPojam.ToString();
            PROVJERI_SLOVO.Enabled = false;
            MessageBox.Show("IZGUBLJENO!", "KRAJ IGRE");
            
        }

        
        private void Form1_Load(object sender, EventArgs e)
        {
            //ucitavanje pojmova iz datoteke u listu:
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine())!= null)
                {
                   ListaPojmova.Add(line);
                }

            }
            PROVJERI_SLOVO.Enabled = false;
            pokazi_pojam.Enabled = false;
        }

        private void PROVJERI_SLOVO_Click(object sender, EventArgs e)
        {
           
            if (tb_uneseno_slovo.Text == "")
            {
                MessageBox.Show("Molim Vas, unesite slovo ili riječ!", "ERROR");
            } 
            else
            {
                tb_iskoristena_slova.Text = tb_iskoristena_slova.Text + " " + tb_uneseno_slovo.Text;
                if (tb_uneseno_slovo.Text.Length == 1)
                {
                    if (brojPokusaja <= 0)
                    {
                        MessageBox.Show("IZGUBLJENO!", "KRAJ IGRE");
                        PROVJERI_SLOVO.Enabled = false;
                        pokazi_pojam.Enabled = false;
                        tb_rjesenje_vjesala.Text = NovoGenenriraniPojam.ToString();

                    }

                    SLOVO = Convert.ToChar(tb_uneseno_slovo.Text);

                    for (int i = 0; i < br; i++)
                    {
                        if (SLOVO == odvojenaslova[i])
                        {
                            for (int j = 0; j < br; j++)
                            {
                                if (j == i)
                                {
                                    privremeniRez[j] = SLOVO;
                                }
                            }
                        }
                        rez = new string(privremeniRez);
                        tb_moj_pogodak.Text = rez;
                    }
                    brojPokusaja--;
                    tb_preostali_pokusaji.Text = brojPokusaja.ToString();
                    if (rez == NovoGenenriraniPojam)
                    {
                        MessageBox.Show("POGODJENO! SVAKA ČAST!", "KRAJ IGRE");
                        PROVJERI_SLOVO.Enabled = false;
                        pokazi_pojam.Enabled = false;
                        tb_rjesenje_vjesala.Text = NovoGenenriraniPojam.ToString();
                    }
                }
                else
                {
                    if (tb_uneseno_slovo.Text == NovoGenenriraniPojam)
                    {
                        MessageBox.Show("POGODJENO! SVAKA ČAST!", "KRAJ IGRE");
                        PROVJERI_SLOVO.Enabled = false;
                        pokazi_pojam.Enabled = false;
                        tb_rjesenje_vjesala.Text = NovoGenenriraniPojam.ToString();
                    }
                    else
                    {
                        if (brojPokusaja <= 0)
                        {
                            MessageBox.Show("IZGUBLJENO!", "KRAJ IGRE");
                            PROVJERI_SLOVO.Enabled = false;
                            pokazi_pojam.Enabled = false;
                            tb_rjesenje_vjesala.Text = NovoGenenriraniPojam.ToString();
                        }
                        brojPokusaja--;
                        tb_preostali_pokusaji.Text = brojPokusaja.ToString();
                        tb_iskoristena_slova.Text = (tb_uneseno_slovo.Text+ " ").ToString();
                        
                    }
                }
            }
        }

        private void NOVI_POJAM_Click(object sender, EventArgs e)
        {
            tb_rjesenje_vjesala.Text = "";
            tb_uneseno_slovo.Text = "";
            tb_moj_pogodak.Text = "";
            tb_iskoristena_slova.Text = "";

            brojPokusaja = 6;
            tb_preostali_pokusaji.Text = brojPokusaja.ToString();
            index = r.Next(ListaPojmova.Count);
            NovoGenenriraniPojam = ListaPojmova[index];
            skriveniString = new string('X', NovoGenenriraniPojam.Length);
            tb_radi_gumb_generiraj_novi_pojam.Text = skriveniString.ToString();

            br = NovoGenenriraniPojam.Length;
            privremeniRez = new char[br];
            skrivenaslova = skriveniString.ToCharArray();
            privremeniRez = skrivenaslova;
            odvojenaslova = NovoGenenriraniPojam.ToCharArray();

            PROVJERI_SLOVO.Enabled = true;
            pokazi_pojam.Enabled = true;
            tb_iskoristena_slova.Text = " ";
        }


    }
}
