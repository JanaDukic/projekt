﻿namespace LV6_OOP_analiza_zd_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.NOVI_POJAM = new System.Windows.Forms.Button();
            this.tb_radi_gumb_generiraj_novi_pojam = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.PROVJERI_SLOVO = new System.Windows.Forms.Button();
            this.tb_uneseno_slovo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_moj_pogodak = new System.Windows.Forms.TextBox();
            this.pokazi_pojam = new System.Windows.Forms.Button();
            this.tb_rjesenje_vjesala = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tb_preostali_pokusaji = new System.Windows.Forms.TextBox();
            this.tb_iskoristena_slova = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(67, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 0;
            // 
            // NOVI_POJAM
            // 
            this.NOVI_POJAM.Location = new System.Drawing.Point(70, 44);
            this.NOVI_POJAM.Name = "NOVI_POJAM";
            this.NOVI_POJAM.Size = new System.Drawing.Size(96, 60);
            this.NOVI_POJAM.TabIndex = 1;
            this.NOVI_POJAM.Text = "GENERIRAJ NOVI POJAM";
            this.NOVI_POJAM.UseVisualStyleBackColor = true;
            this.NOVI_POJAM.Click += new System.EventHandler(this.NOVI_POJAM_Click);
            // 
            // tb_radi_gumb_generiraj_novi_pojam
            // 
            this.tb_radi_gumb_generiraj_novi_pojam.Location = new System.Drawing.Point(195, 84);
            this.tb_radi_gumb_generiraj_novi_pojam.Name = "tb_radi_gumb_generiraj_novi_pojam";
            this.tb_radi_gumb_generiraj_novi_pojam.Size = new System.Drawing.Size(180, 20);
            this.tb_radi_gumb_generiraj_novi_pojam.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(220, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "NOVI POJAM:";
            // 
            // PROVJERI_SLOVO
            // 
            this.PROVJERI_SLOVO.Location = new System.Drawing.Point(73, 210);
            this.PROVJERI_SLOVO.Name = "PROVJERI_SLOVO";
            this.PROVJERI_SLOVO.Size = new System.Drawing.Size(96, 75);
            this.PROVJERI_SLOVO.TabIndex = 4;
            this.PROVJERI_SLOVO.Text = "PROVJERI TO SLOVO";
            this.PROVJERI_SLOVO.UseVisualStyleBackColor = true;
            this.PROVJERI_SLOVO.Click += new System.EventHandler(this.PROVJERI_SLOVO_Click);
            // 
            // tb_uneseno_slovo
            // 
            this.tb_uneseno_slovo.Location = new System.Drawing.Point(185, 238);
            this.tb_uneseno_slovo.Name = "tb_uneseno_slovo";
            this.tb_uneseno_slovo.Size = new System.Drawing.Size(190, 20);
            this.tb_uneseno_slovo.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(202, 210);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "UNESI SLOVO:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(409, 210);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "VAŠ POGODAK:";
            // 
            // tb_moj_pogodak
            // 
            this.tb_moj_pogodak.Location = new System.Drawing.Point(381, 238);
            this.tb_moj_pogodak.Name = "tb_moj_pogodak";
            this.tb_moj_pogodak.Size = new System.Drawing.Size(196, 20);
            this.tb_moj_pogodak.TabIndex = 8;
            // 
            // pokazi_pojam
            // 
            this.pokazi_pojam.Location = new System.Drawing.Point(390, 44);
            this.pokazi_pojam.Name = "pokazi_pojam";
            this.pokazi_pojam.Size = new System.Drawing.Size(103, 64);
            this.pokazi_pojam.TabIndex = 9;
            this.pokazi_pojam.Text = "PRIKAŽI RJEŠENJE";
            this.pokazi_pojam.UseVisualStyleBackColor = true;
            this.pokazi_pojam.Click += new System.EventHandler(this.pokazi_pojam_Click);
            // 
            // tb_rjesenje_vjesala
            // 
            this.tb_rjesenje_vjesala.Location = new System.Drawing.Point(520, 84);
            this.tb_rjesenje_vjesala.Name = "tb_rjesenje_vjesala";
            this.tb_rjesenje_vjesala.Size = new System.Drawing.Size(200, 20);
            this.tb_rjesenje_vjesala.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(551, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(113, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "RJEŠENJE VJEŠALA:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(595, 210);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(125, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "PREOSTALI POKUŠAJI:";
            // 
            // tb_preostali_pokusaji
            // 
            this.tb_preostali_pokusaji.Location = new System.Drawing.Point(607, 238);
            this.tb_preostali_pokusaji.Name = "tb_preostali_pokusaji";
            this.tb_preostali_pokusaji.Size = new System.Drawing.Size(100, 20);
            this.tb_preostali_pokusaji.TabIndex = 13;
            // 
            // tb_iskoristena_slova
            // 
            this.tb_iskoristena_slova.Location = new System.Drawing.Point(235, 358);
            this.tb_iskoristena_slova.Name = "tb_iskoristena_slova";
            this.tb_iskoristena_slova.Size = new System.Drawing.Size(452, 20);
            this.tb_iskoristena_slova.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(81, 365);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "ISKORIŠTENA SLOVA:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(676, 406);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 32);
            this.button1.TabIndex = 16;
            this.button1.Text = "QUIT";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tb_iskoristena_slova);
            this.Controls.Add(this.tb_preostali_pokusaji);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tb_rjesenje_vjesala);
            this.Controls.Add(this.pokazi_pojam);
            this.Controls.Add(this.tb_moj_pogodak);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tb_uneseno_slovo);
            this.Controls.Add(this.PROVJERI_SLOVO);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb_radi_gumb_generiraj_novi_pojam);
            this.Controls.Add(this.NOVI_POJAM);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button NOVI_POJAM;
        private System.Windows.Forms.TextBox tb_radi_gumb_generiraj_novi_pojam;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button PROVJERI_SLOVO;
        private System.Windows.Forms.TextBox tb_uneseno_slovo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_moj_pogodak;
        private System.Windows.Forms.Button pokazi_pojam;
        private System.Windows.Forms.TextBox tb_rjesenje_vjesala;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tb_preostali_pokusaji;
        private System.Windows.Forms.TextBox tb_iskoristena_slova;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button1;
    }
}

