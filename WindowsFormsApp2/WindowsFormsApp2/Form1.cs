﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class X_O : Form
    {
        bool redZaIgru = true; // igra X
        int brojPoteza = 0;

        Graphics g;
        Pen pen = new Pen(Color.Blue);
        string Player1_tool;
        string Player2_tool;

      
        public X_O()
        {
            InitializeComponent();
            RaddioBattonX.Checked = true;
            button1.Enabled = true;

            A1.Enabled = false;
            A1.Text = "";
            A2.Enabled = false;
            A2.Text = "";
            A3.Enabled = false;
            A3.Text = "";

            B1.Enabled = false;
            B1.Text = "";
            B2.Enabled = false;
            B2.Text = "";
            B3.Enabled = false;
            B3.Text = "";

            C1.Enabled = false;
            C1.Text = "";
            C2.Enabled = false;
            C2.Text = "";
            C3.Enabled = false;
            C3.Text = "";
        }
           
        
         
        private void Form1_Load(object sender, EventArgs e)
        {
            textBox_Player1.Text = "";
            textBox_Player2.Text = "";
            
            
        }

        private void button_START_GAME_Click(object sender, EventArgs e)
        {
            if (textBox_Player1.Text == "")
            {
                MessageBox.Show("Molim unesite naziv prvog igrača", "Pogreška");
            }
            else if (textBox_Player2.Text == "")
            {
                MessageBox.Show("Molim unesite naziv drugog igrača", "Pogreška");
            }
            else if (Player1_tool == "X")
            {
                
                textBox1.Text = (textBox1.Text + "Prvi igra " +textBox_Player1.Text + "\r\n").ToString();
                //button_START_GAME.Enabled = false;
            }
            else if (Player1_tool == "O")
            {
                
                textBox1.Text = (textBox1.Text + "Prvi igra "+ textBox_Player2.Text + "\r\n").ToString();
                //button_START_GAME.Enabled = false;
            }
                 
                redZaIgru = true;
                brojPoteza = 0;
            //try
            //{
            //    foreach (Control c in Controls)
            //    {
            //        Button b = (Button)c;
            //        b.Enabled = true;
            //        b.Text = "";
            //    }
            //}
            //catch { }

            A1.Enabled = true;
            A1.Text = "";
            A2.Enabled = true;
            A2.Text = "";
            A3.Enabled = true;
            A3.Text = "";

            B1.Enabled = true;
            B1.Text = "";
            B2.Enabled = true;
            B2.Text = "";
            B3.Enabled = true;
            B3.Text = "";

            C1.Enabled = true;
            C1.Text = "";
            C2.Enabled = true;
            C2.Text = "";
            C3.Enabled = true;
            C3.Text = "";

        }

        private void RaddioBattonX_CheckedChanged(object sender, EventArgs e)
        {
            if (RaddioBattonX.Checked)
            {
                Player1_tool = "X";
                Player2_tool = "O";
            }
                       
        }

        private void RB_O_CheckedChanged(object sender, EventArgs e)
        {
            if (RB_O.Checked)
            {
                Player1_tool = "O";
                Player2_tool = "X";
            }
           
        }

        

        private void Button_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (redZaIgru)
            {
                b.Text = "X";
            }
            else
            {
                b.Text = "O";
            }
            redZaIgru = !redZaIgru;
            b.Enabled = false;
            brojPoteza++;
            Pobjednik();
           
        }

        private void Pobjednik()
        {
            bool zastavica = false;
            if((A1.Text == A2.Text) && (A2.Text == A3.Text) &&(!A1.Enabled))
            {
                zastavica = true;
            }
            else if ((B1.Text == B2.Text) && (B2.Text == B3.Text) && (!B1.Enabled))
            {
                zastavica = true;
            }
            else if ((C1.Text == C2.Text) && (C2.Text == C3.Text) && (!C1.Enabled))
            {
                zastavica = true;
            }

            if ((A1.Text == B1.Text) && (B1.Text == C1.Text) && (!A1.Enabled))
            {
                zastavica = true;
            }
            else if ((A2.Text == B2.Text) && (B2.Text == C2.Text) && (!B2.Enabled))
            {
                zastavica = true;
            }
            else if ((A3.Text == B3.Text) && (B3.Text == C3.Text) && (!C3.Enabled))
            {
                zastavica = true;
            }

            if ((A1.Text == B2.Text) && (B2.Text == C3.Text) && (!A1.Enabled))
            {
                zastavica = true;
            }
            else if ((A3.Text == B2.Text) && (B2.Text == C1.Text) && (!C1.Enabled))
            {
                zastavica = true;
            }

            if (zastavica == true)
            {
                OnesposobiOstaleGumbe();
                string pobjednik = "";
                if (redZaIgru)
                {
                    pobjednik = "O";
                    if (Player1_tool == pobjednik)
                    {
                        MessageBox.Show(textBox_Player1.Text + " je pobjedio!", "Pobjeda!");
                    }
                    else
                    {
                        MessageBox.Show(textBox_Player2.Text + " je pobjedio!", "Pobjeda!");
                    }
                }
                else
                {
                    pobjednik = "X";
                    if (Player1_tool == pobjednik)
                    {
                        MessageBox.Show(textBox_Player1.Text + " je pobjedio!", "Pobjeda!");
                    }
                    else
                    {
                        MessageBox.Show(textBox_Player2.Text + " je pobjedio!", "Pobjeda!");
                    }
                    
                }
                textBox1.Text = (textBox1.Text + "\r------------------------\r\n").ToString();
                
            }
            else
            {
                if(brojPoteza == 9)
                {
                    MessageBox.Show("Ops, nitko nije pobijedio!", "Nerješeno!");
                }
            }
        }
        private void OnesposobiOstaleGumbe()
        {
            //try
            //{
            //    foreach (Control c in Controls)
            //    {
            //        Button b = (Button)c;
            //        b.Enabled = false;
            //    }
            //}
            //catch { }

            A1.Enabled = false;
            
            A2.Enabled = false;
            
            A3.Enabled = false;
           

            B1.Enabled = false;
           
            B2.Enabled = false;
           
            B3.Enabled = false;
           

            C1.Enabled = false;
            
            C2.Enabled = false;
            
            C3.Enabled = false;
            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
