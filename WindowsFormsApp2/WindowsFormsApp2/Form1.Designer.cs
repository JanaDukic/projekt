﻿namespace WindowsFormsApp2
{
    partial class X_O
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_Player1 = new System.Windows.Forms.Label();
            this.label_Player2 = new System.Windows.Forms.Label();
            this.textBox_Player1 = new System.Windows.Forms.TextBox();
            this.textBox_Player2 = new System.Windows.Forms.TextBox();
            this.button_START_GAME = new System.Windows.Forms.Button();
            this.RaddioBattonX = new System.Windows.Forms.RadioButton();
            this.RB_O = new System.Windows.Forms.RadioButton();
            this.groupBox = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.A1 = new System.Windows.Forms.Button();
            this.A2 = new System.Windows.Forms.Button();
            this.A3 = new System.Windows.Forms.Button();
            this.B1 = new System.Windows.Forms.Button();
            this.B2 = new System.Windows.Forms.Button();
            this.B3 = new System.Windows.Forms.Button();
            this.C1 = new System.Windows.Forms.Button();
            this.C2 = new System.Windows.Forms.Button();
            this.C3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // label_Player1
            // 
            this.label_Player1.AutoSize = true;
            this.label_Player1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_Player1.Location = new System.Drawing.Point(45, 27);
            this.label_Player1.Name = "label_Player1";
            this.label_Player1.Size = new System.Drawing.Size(61, 16);
            this.label_Player1.TabIndex = 0;
            this.label_Player1.Text = "Player1";
            // 
            // label_Player2
            // 
            this.label_Player2.AutoSize = true;
            this.label_Player2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_Player2.Location = new System.Drawing.Point(197, 27);
            this.label_Player2.Name = "label_Player2";
            this.label_Player2.Size = new System.Drawing.Size(61, 16);
            this.label_Player2.TabIndex = 1;
            this.label_Player2.Text = "Player2";
            // 
            // textBox_Player1
            // 
            this.textBox_Player1.Location = new System.Drawing.Point(48, 46);
            this.textBox_Player1.Name = "textBox_Player1";
            this.textBox_Player1.Size = new System.Drawing.Size(112, 20);
            this.textBox_Player1.TabIndex = 2;
            // 
            // textBox_Player2
            // 
            this.textBox_Player2.Location = new System.Drawing.Point(200, 46);
            this.textBox_Player2.Name = "textBox_Player2";
            this.textBox_Player2.Size = new System.Drawing.Size(112, 20);
            this.textBox_Player2.TabIndex = 3;
            // 
            // button_START_GAME
            // 
            this.button_START_GAME.Location = new System.Drawing.Point(473, 312);
            this.button_START_GAME.Name = "button_START_GAME";
            this.button_START_GAME.Size = new System.Drawing.Size(176, 40);
            this.button_START_GAME.TabIndex = 4;
            this.button_START_GAME.Text = "PRESS to START";
            this.button_START_GAME.UseVisualStyleBackColor = true;
            this.button_START_GAME.Click += new System.EventHandler(this.button_START_GAME_Click);
            // 
            // RaddioBattonX
            // 
            this.RaddioBattonX.AutoSize = true;
            this.RaddioBattonX.Location = new System.Drawing.Point(18, 19);
            this.RaddioBattonX.Name = "RaddioBattonX";
            this.RaddioBattonX.Size = new System.Drawing.Size(54, 17);
            this.RaddioBattonX.TabIndex = 8;
            this.RaddioBattonX.TabStop = true;
            this.RaddioBattonX.Text = "X (iks)\r\n";
            this.RaddioBattonX.UseVisualStyleBackColor = true;
            this.RaddioBattonX.CheckedChanged += new System.EventHandler(this.RaddioBattonX_CheckedChanged);
            // 
            // RB_O
            // 
            this.RB_O.AutoSize = true;
            this.RB_O.Location = new System.Drawing.Point(18, 42);
            this.RB_O.Name = "RB_O";
            this.RB_O.Size = new System.Drawing.Size(59, 17);
            this.RB_O.TabIndex = 9;
            this.RB_O.TabStop = true;
            this.RB_O.Text = "O (oks)\r\n";
            this.RB_O.UseVisualStyleBackColor = true;
            this.RB_O.CheckedChanged += new System.EventHandler(this.RB_O_CheckedChanged);
            // 
            // groupBox
            // 
            this.groupBox.Controls.Add(this.RaddioBattonX);
            this.groupBox.Controls.Add(this.RB_O);
            this.groupBox.Location = new System.Drawing.Point(23, 72);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(163, 79);
            this.groupBox.TabIndex = 10;
            this.groupBox.TabStop = false;
            this.groupBox.Text = "Choose your weapon Player1:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(23, 173);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(253, 240);
            this.textBox1.TabIndex = 11;
            // 
            // A1
            // 
            this.A1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.A1.Location = new System.Drawing.Point(384, 27);
            this.A1.Name = "A1";
            this.A1.Size = new System.Drawing.Size(80, 80);
            this.A1.TabIndex = 12;
            this.A1.UseVisualStyleBackColor = true;
            this.A1.Click += new System.EventHandler(this.Button_Click);
            // 
            // A2
            // 
            this.A2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.A2.Location = new System.Drawing.Point(470, 27);
            this.A2.Name = "A2";
            this.A2.Size = new System.Drawing.Size(80, 80);
            this.A2.TabIndex = 13;
            this.A2.UseVisualStyleBackColor = true;
            this.A2.Click += new System.EventHandler(this.Button_Click);
            // 
            // A3
            // 
            this.A3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.A3.Location = new System.Drawing.Point(556, 28);
            this.A3.Name = "A3";
            this.A3.Size = new System.Drawing.Size(80, 80);
            this.A3.TabIndex = 14;
            this.A3.UseVisualStyleBackColor = true;
            this.A3.Click += new System.EventHandler(this.Button_Click);
            // 
            // B1
            // 
            this.B1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.B1.Location = new System.Drawing.Point(384, 113);
            this.B1.Name = "B1";
            this.B1.Size = new System.Drawing.Size(80, 80);
            this.B1.TabIndex = 15;
            this.B1.UseVisualStyleBackColor = true;
            this.B1.Click += new System.EventHandler(this.Button_Click);
            // 
            // B2
            // 
            this.B2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.B2.Location = new System.Drawing.Point(470, 114);
            this.B2.Name = "B2";
            this.B2.Size = new System.Drawing.Size(80, 80);
            this.B2.TabIndex = 16;
            this.B2.UseVisualStyleBackColor = true;
            this.B2.Click += new System.EventHandler(this.Button_Click);
            // 
            // B3
            // 
            this.B3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.B3.Location = new System.Drawing.Point(556, 114);
            this.B3.Name = "B3";
            this.B3.Size = new System.Drawing.Size(80, 80);
            this.B3.TabIndex = 17;
            this.B3.UseVisualStyleBackColor = true;
            this.B3.Click += new System.EventHandler(this.Button_Click);
            // 
            // C1
            // 
            this.C1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.C1.Location = new System.Drawing.Point(384, 199);
            this.C1.Name = "C1";
            this.C1.Size = new System.Drawing.Size(80, 80);
            this.C1.TabIndex = 18;
            this.C1.UseVisualStyleBackColor = true;
            this.C1.Click += new System.EventHandler(this.Button_Click);
            // 
            // C2
            // 
            this.C2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.C2.Location = new System.Drawing.Point(470, 200);
            this.C2.Name = "C2";
            this.C2.Size = new System.Drawing.Size(80, 80);
            this.C2.TabIndex = 19;
            this.C2.UseVisualStyleBackColor = true;
            this.C2.Click += new System.EventHandler(this.Button_Click);
            // 
            // C3
            // 
            this.C3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.C3.Location = new System.Drawing.Point(556, 199);
            this.C3.Name = "C3";
            this.C3.Size = new System.Drawing.Size(80, 80);
            this.C3.TabIndex = 20;
            this.C3.UseVisualStyleBackColor = true;
            this.C3.Click += new System.EventHandler(this.Button_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(691, 368);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 21;
            this.button1.Text = "EXIT";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // X_O
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.C3);
            this.Controls.Add(this.C2);
            this.Controls.Add(this.C1);
            this.Controls.Add(this.B3);
            this.Controls.Add(this.B2);
            this.Controls.Add(this.B1);
            this.Controls.Add(this.A3);
            this.Controls.Add(this.A2);
            this.Controls.Add(this.A1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.groupBox);
            this.Controls.Add(this.button_START_GAME);
            this.Controls.Add(this.textBox_Player2);
            this.Controls.Add(this.textBox_Player1);
            this.Controls.Add(this.label_Player2);
            this.Controls.Add(this.label_Player1);
            this.Name = "X_O";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox.ResumeLayout(false);
            this.groupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_Player1;
        private System.Windows.Forms.Label label_Player2;
        private System.Windows.Forms.TextBox textBox_Player1;
        private System.Windows.Forms.TextBox textBox_Player2;
        private System.Windows.Forms.Button button_START_GAME;
        private System.Windows.Forms.RadioButton RaddioBattonX;
        private System.Windows.Forms.RadioButton RB_O;
        private System.Windows.Forms.GroupBox groupBox;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button A1;
        private System.Windows.Forms.Button A2;
        private System.Windows.Forms.Button A3;
        private System.Windows.Forms.Button B1;
        private System.Windows.Forms.Button B2;
        private System.Windows.Forms.Button B3;
        private System.Windows.Forms.Button C1;
        private System.Windows.Forms.Button C2;
        private System.Windows.Forms.Button C3;
        private System.Windows.Forms.Button button1;
    }
}

