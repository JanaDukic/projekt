﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Analiza_lv6_zd1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        double operand1, operand2;

        private void UnosOperanda1_TextChanged(object sender, EventArgs e)
        {

        }

        private void ZBROJ_Click(object sender, EventArgs e)
        {
            if(!double.TryParse(UnosOperanda1.Text, out operand1))
            {
                MessageBox.Show("POGREŠAN UNOS OPERANDA 1!", "Pogreška");
            }
            else if(!double.TryParse(UnosOperanda2.Text, out operand2))
            {
                MessageBox.Show("POGREŠAN UNOS OPERANDA 2!", "Pogreška");
            }
            else
            {
                rezZBRAJANJA.Text = (operand1 + operand2).ToString();
            }
        }

        private void ODUZIMANJE_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(UnosOperanda1.Text, out operand1))
            {
                MessageBox.Show("POGREŠAN UNOS OPERANDA 1!", "Pogreška");
            }
            else if (!double.TryParse(UnosOperanda2.Text, out operand2))
            {
                MessageBox.Show("POGREŠAN UNOS OPERANDA 2!", "Pogreška");
            }
            else
            {
                rezODUZIMANJA.Text = (operand1 - operand2).ToString();
            }
        }

        private void rezZBRAJANJA_TextChanged(object sender, EventArgs e)
        {

        }

        private void rezODUZIMANJA_TextChanged(object sender, EventArgs e)
        {

        }

        private void rezMNOZENJA_TextChanged(object sender, EventArgs e)
        {

        }

        private void rezSINUSA_TextChanged(object sender, EventArgs e)
        {

        }

        private void rezCOSINUSA_TextChanged(object sender, EventArgs e)
        {

        }

        private void rezKORJENOVANJA_TextChanged(object sender, EventArgs e)
        {

        }

        private void MNOZENJE_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(UnosOperanda1.Text, out operand1))
            {
                MessageBox.Show("POGREŠAN UNOS OPERANDA 1!", "Pogreška");
            }
            else if (!double.TryParse(UnosOperanda2.Text, out operand2))
            {
                MessageBox.Show("POGREŠAN UNOS OPERANDA 2!", "Pogreška");
            }
            else
            {
                rezMNOZENJA.Text = (operand1 * operand2).ToString();
            }
        }

        private void DIJELJENJE_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(UnosOperanda1.Text, out operand1))
            {
                MessageBox.Show("POGREŠAN UNOS OPERANDA 1!", "Pogreška");
            }
            else if (!double.TryParse(UnosOperanda2.Text, out operand2))
            {
                MessageBox.Show("POGREŠAN UNOS OPERANDA 2!", "Pogreška");
            }
            else
            {
                rezDIJELJENJA.Text = (operand1 / operand2).ToString();
            }
        }

        private void SINUS_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(UnosOperanda1.Text, out operand1))
            {
                MessageBox.Show("POGREŠAN UNOS OPERANDA 1!", "Pogreška");
            }
            else if (!double.TryParse(UnosOperanda2.Text, out operand2))
            {
                MessageBox.Show("POGREŠAN UNOS OPERANDA 2!", "Pogreška");
            }
            else
            {
                rezSINUSA.Text = (Math.Sin(operand1) + ", " + Math.Sin(operand2)).ToString();
            }
        }

        private void COSINUS_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(UnosOperanda1.Text, out operand1))
            {
                MessageBox.Show("POGREŠAN UNOS OPERANDA 1!", "Pogreška");
            }
            else if (!double.TryParse(UnosOperanda2.Text, out operand2))
            {
                MessageBox.Show("POGREŠAN UNOS OPERANDA 2!", "Pogreška");
            }
            else
            {
                rezCOSINUSA.Text = (Math.Cos(operand1) + ", " + Math.Cos(operand2)).ToString();
            }
        }

        private void LOGARITAM_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(UnosOperanda1.Text, out operand1))
            {
                MessageBox.Show("POGREŠAN UNOS OPERANDA 1!", "Pogreška");
            }
            else if (!double.TryParse(UnosOperanda2.Text, out operand2))
            {
                if (operand2 < 0)
                {
                    MessageBox.Show("operand za LOGARITMIRANJE mora biti >0!", "Pogreška");
                }
                else
                    MessageBox.Show("POGREŠAN UNOS OPERANDA 2!", "Pogreška");
            }
            else if (operand1 < 0 || operand2 < 0)
            {
                MessageBox.Show("operandi za LOGARITMIRANJE moraju biti >0!", "Pogreška");
            }else {
                rezLOGARITMIRANJA.Text = (Math.Log10(operand1) + ", " + Math.Log10(operand2)).ToString();
            }
        }

        private void KORJENOVANJE_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(UnosOperanda1.Text, out operand1))
            { 
                MessageBox.Show("POGREŠAN UNOS OPERANDA 1!", "Pogreška");
            }
            else if (!double.TryParse(UnosOperanda2.Text, out operand2))
            {
                MessageBox.Show("POGREŠAN UNOS OPERANDA 2!", "Pogreška");
            }
            else if (operand1 < 0 || operand2 < 0)
            {
                MessageBox.Show("operandi za KORJENOVANJE moraju biti >0 jer ovaj kalkulator ne radi s kompleksnim brojevima", "Pogreška");
            }
            else
            {
                rezKORJENOVANJA.Text = (Math.Sqrt(operand1) + ", " + Math.Sqrt(operand2)).ToString();
            }
        }

        private void KVADRIRAJ_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(UnosOperanda1.Text, out operand1))
            {
                MessageBox.Show("POGREŠAN UNOS OPERANDA 1!", "Pogreška");
            }
            else if (!double.TryParse(UnosOperanda2.Text, out operand2))
            {
                MessageBox.Show("POGREŠAN UNOS OPERANDA 2!", "Pogreška");
            }
            else
            {
                rezKVADRIRANJA.Text = (operand1*operand1 + ", " + operand2*operand2).ToString();
            }
        }

        private void quit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void rezLOGARITMIRANJA_TextChanged(object sender, EventArgs e)
        {

        }

        private void rezKVADRIRANJA_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
