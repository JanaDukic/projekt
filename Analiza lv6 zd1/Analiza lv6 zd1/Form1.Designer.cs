﻿namespace Analiza_lv6_zd1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.UnosOperanda1 = new System.Windows.Forms.TextBox();
            this.UnosOperanda2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ZBROJ = new System.Windows.Forms.Button();
            this.ODUZIMANJE = new System.Windows.Forms.Button();
            this.MNOZENJE = new System.Windows.Forms.Button();
            this.DIJELJENJE = new System.Windows.Forms.Button();
            this.SINUS = new System.Windows.Forms.Button();
            this.COSINUS = new System.Windows.Forms.Button();
            this.LOGARITAM = new System.Windows.Forms.Button();
            this.KORJENOVANJE = new System.Windows.Forms.Button();
            this.KVADRIRAJ = new System.Windows.Forms.Button();
            this.REZULTATI = new System.Windows.Forms.Label();
            this.rezZBRAJANJA = new System.Windows.Forms.TextBox();
            this.rezODUZIMANJA = new System.Windows.Forms.TextBox();
            this.rezMNOZENJA = new System.Windows.Forms.TextBox();
            this.rezDIJELJENJA = new System.Windows.Forms.TextBox();
            this.rezSINUSA = new System.Windows.Forms.TextBox();
            this.rezCOSINUSA = new System.Windows.Forms.TextBox();
            this.rezLOGARITMIRANJA = new System.Windows.Forms.TextBox();
            this.rezKORJENOVANJA = new System.Windows.Forms.TextBox();
            this.rezKVADRIRANJA = new System.Windows.Forms.TextBox();
            this.quit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Unesi prvi operand:";
            // 
            // UnosOperanda1
            // 
            this.UnosOperanda1.Location = new System.Drawing.Point(141, 61);
            this.UnosOperanda1.Name = "UnosOperanda1";
            this.UnosOperanda1.Size = new System.Drawing.Size(100, 20);
            this.UnosOperanda1.TabIndex = 1;
            this.UnosOperanda1.TextChanged += new System.EventHandler(this.UnosOperanda1_TextChanged);
            // 
            // UnosOperanda2
            // 
            this.UnosOperanda2.Location = new System.Drawing.Point(141, 96);
            this.UnosOperanda2.Name = "UnosOperanda2";
            this.UnosOperanda2.Size = new System.Drawing.Size(100, 20);
            this.UnosOperanda2.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Unos drugi operand:";
            // 
            // ZBROJ
            // 
            this.ZBROJ.Location = new System.Drawing.Point(313, 63);
            this.ZBROJ.Name = "ZBROJ";
            this.ZBROJ.Size = new System.Drawing.Size(75, 23);
            this.ZBROJ.TabIndex = 4;
            this.ZBROJ.Text = "ZBROJI";
            this.ZBROJ.UseVisualStyleBackColor = true;
            this.ZBROJ.Click += new System.EventHandler(this.ZBROJ_Click);
            // 
            // ODUZIMANJE
            // 
            this.ODUZIMANJE.Location = new System.Drawing.Point(313, 98);
            this.ODUZIMANJE.Name = "ODUZIMANJE";
            this.ODUZIMANJE.Size = new System.Drawing.Size(75, 23);
            this.ODUZIMANJE.TabIndex = 5;
            this.ODUZIMANJE.Text = "ODUZMI";
            this.ODUZIMANJE.UseVisualStyleBackColor = true;
            this.ODUZIMANJE.Click += new System.EventHandler(this.ODUZIMANJE_Click);
            // 
            // MNOZENJE
            // 
            this.MNOZENJE.Location = new System.Drawing.Point(313, 137);
            this.MNOZENJE.Name = "MNOZENJE";
            this.MNOZENJE.Size = new System.Drawing.Size(75, 23);
            this.MNOZENJE.TabIndex = 6;
            this.MNOZENJE.Text = "POMNOZI";
            this.MNOZENJE.UseVisualStyleBackColor = true;
            this.MNOZENJE.Click += new System.EventHandler(this.MNOZENJE_Click);
            // 
            // DIJELJENJE
            // 
            this.DIJELJENJE.Location = new System.Drawing.Point(313, 175);
            this.DIJELJENJE.Name = "DIJELJENJE";
            this.DIJELJENJE.Size = new System.Drawing.Size(75, 23);
            this.DIJELJENJE.TabIndex = 7;
            this.DIJELJENJE.Text = "PODIJELI";
            this.DIJELJENJE.UseVisualStyleBackColor = true;
            this.DIJELJENJE.Click += new System.EventHandler(this.DIJELJENJE_Click);
            // 
            // SINUS
            // 
            this.SINUS.Location = new System.Drawing.Point(313, 227);
            this.SINUS.Name = "SINUS";
            this.SINUS.Size = new System.Drawing.Size(75, 23);
            this.SINUS.TabIndex = 8;
            this.SINUS.Text = "SIN";
            this.SINUS.UseVisualStyleBackColor = true;
            this.SINUS.Click += new System.EventHandler(this.SINUS_Click);
            // 
            // COSINUS
            // 
            this.COSINUS.Location = new System.Drawing.Point(313, 266);
            this.COSINUS.Name = "COSINUS";
            this.COSINUS.Size = new System.Drawing.Size(75, 23);
            this.COSINUS.TabIndex = 9;
            this.COSINUS.Text = "COS";
            this.COSINUS.UseVisualStyleBackColor = true;
            this.COSINUS.Click += new System.EventHandler(this.COSINUS_Click);
            // 
            // LOGARITAM
            // 
            this.LOGARITAM.Location = new System.Drawing.Point(313, 306);
            this.LOGARITAM.Name = "LOGARITAM";
            this.LOGARITAM.Size = new System.Drawing.Size(75, 23);
            this.LOGARITAM.TabIndex = 10;
            this.LOGARITAM.Text = "LOG";
            this.LOGARITAM.UseVisualStyleBackColor = true;
            this.LOGARITAM.Click += new System.EventHandler(this.LOGARITAM_Click);
            // 
            // KORJENOVANJE
            // 
            this.KORJENOVANJE.Location = new System.Drawing.Point(313, 344);
            this.KORJENOVANJE.Name = "KORJENOVANJE";
            this.KORJENOVANJE.Size = new System.Drawing.Size(75, 23);
            this.KORJENOVANJE.TabIndex = 11;
            this.KORJENOVANJE.Text = "SQRT";
            this.KORJENOVANJE.UseVisualStyleBackColor = true;
            this.KORJENOVANJE.Click += new System.EventHandler(this.KORJENOVANJE_Click);
            // 
            // KVADRIRAJ
            // 
            this.KVADRIRAJ.Location = new System.Drawing.Point(313, 382);
            this.KVADRIRAJ.Name = "KVADRIRAJ";
            this.KVADRIRAJ.Size = new System.Drawing.Size(75, 23);
            this.KVADRIRAJ.TabIndex = 12;
            this.KVADRIRAJ.Text = "^2";
            this.KVADRIRAJ.UseVisualStyleBackColor = true;
            this.KVADRIRAJ.Click += new System.EventHandler(this.KVADRIRAJ_Click);
            // 
            // REZULTATI
            // 
            this.REZULTATI.AutoSize = true;
            this.REZULTATI.Location = new System.Drawing.Point(429, 34);
            this.REZULTATI.Name = "REZULTATI";
            this.REZULTATI.Size = new System.Drawing.Size(70, 13);
            this.REZULTATI.TabIndex = 13;
            this.REZULTATI.Text = "REZULTATI:";
            // 
            // rezZBRAJANJA
            // 
            this.rezZBRAJANJA.Location = new System.Drawing.Point(432, 63);
            this.rezZBRAJANJA.Name = "rezZBRAJANJA";
            this.rezZBRAJANJA.Size = new System.Drawing.Size(100, 20);
            this.rezZBRAJANJA.TabIndex = 14;
            this.rezZBRAJANJA.TextChanged += new System.EventHandler(this.rezZBRAJANJA_TextChanged);
            // 
            // rezODUZIMANJA
            // 
            this.rezODUZIMANJA.Location = new System.Drawing.Point(432, 100);
            this.rezODUZIMANJA.Name = "rezODUZIMANJA";
            this.rezODUZIMANJA.Size = new System.Drawing.Size(100, 20);
            this.rezODUZIMANJA.TabIndex = 15;
            this.rezODUZIMANJA.TextChanged += new System.EventHandler(this.rezODUZIMANJA_TextChanged);
            // 
            // rezMNOZENJA
            // 
            this.rezMNOZENJA.Location = new System.Drawing.Point(432, 139);
            this.rezMNOZENJA.Name = "rezMNOZENJA";
            this.rezMNOZENJA.Size = new System.Drawing.Size(100, 20);
            this.rezMNOZENJA.TabIndex = 16;
            this.rezMNOZENJA.TextChanged += new System.EventHandler(this.rezMNOZENJA_TextChanged);
            // 
            // rezDIJELJENJA
            // 
            this.rezDIJELJENJA.Location = new System.Drawing.Point(432, 177);
            this.rezDIJELJENJA.Name = "rezDIJELJENJA";
            this.rezDIJELJENJA.Size = new System.Drawing.Size(100, 20);
            this.rezDIJELJENJA.TabIndex = 17;
            // 
            // rezSINUSA
            // 
            this.rezSINUSA.Location = new System.Drawing.Point(432, 229);
            this.rezSINUSA.Name = "rezSINUSA";
            this.rezSINUSA.Size = new System.Drawing.Size(278, 20);
            this.rezSINUSA.TabIndex = 18;
            this.rezSINUSA.TextChanged += new System.EventHandler(this.rezSINUSA_TextChanged);
            // 
            // rezCOSINUSA
            // 
            this.rezCOSINUSA.Location = new System.Drawing.Point(432, 268);
            this.rezCOSINUSA.Name = "rezCOSINUSA";
            this.rezCOSINUSA.Size = new System.Drawing.Size(278, 20);
            this.rezCOSINUSA.TabIndex = 19;
            this.rezCOSINUSA.TextChanged += new System.EventHandler(this.rezCOSINUSA_TextChanged);
            // 
            // rezLOGARITMIRANJA
            // 
            this.rezLOGARITMIRANJA.Location = new System.Drawing.Point(432, 308);
            this.rezLOGARITMIRANJA.Name = "rezLOGARITMIRANJA";
            this.rezLOGARITMIRANJA.Size = new System.Drawing.Size(278, 20);
            this.rezLOGARITMIRANJA.TabIndex = 20;
            this.rezLOGARITMIRANJA.TextChanged += new System.EventHandler(this.rezLOGARITMIRANJA_TextChanged);
            // 
            // rezKORJENOVANJA
            // 
            this.rezKORJENOVANJA.Location = new System.Drawing.Point(432, 346);
            this.rezKORJENOVANJA.Name = "rezKORJENOVANJA";
            this.rezKORJENOVANJA.Size = new System.Drawing.Size(278, 20);
            this.rezKORJENOVANJA.TabIndex = 21;
            this.rezKORJENOVANJA.TextChanged += new System.EventHandler(this.rezKORJENOVANJA_TextChanged);
            // 
            // rezKVADRIRANJA
            // 
            this.rezKVADRIRANJA.Location = new System.Drawing.Point(432, 384);
            this.rezKVADRIRANJA.Name = "rezKVADRIRANJA";
            this.rezKVADRIRANJA.Size = new System.Drawing.Size(100, 20);
            this.rezKVADRIRANJA.TabIndex = 22;
            this.rezKVADRIRANJA.TextChanged += new System.EventHandler(this.rezKVADRIRANJA_TextChanged);
            // 
            // quit
            // 
            this.quit.Location = new System.Drawing.Point(694, 396);
            this.quit.Name = "quit";
            this.quit.Size = new System.Drawing.Size(75, 23);
            this.quit.TabIndex = 23;
            this.quit.Text = "QUIT";
            this.quit.UseVisualStyleBackColor = true;
            this.quit.Click += new System.EventHandler(this.quit_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.quit);
            this.Controls.Add(this.rezKVADRIRANJA);
            this.Controls.Add(this.rezKORJENOVANJA);
            this.Controls.Add(this.rezLOGARITMIRANJA);
            this.Controls.Add(this.rezCOSINUSA);
            this.Controls.Add(this.rezSINUSA);
            this.Controls.Add(this.rezDIJELJENJA);
            this.Controls.Add(this.rezMNOZENJA);
            this.Controls.Add(this.rezODUZIMANJA);
            this.Controls.Add(this.rezZBRAJANJA);
            this.Controls.Add(this.REZULTATI);
            this.Controls.Add(this.KVADRIRAJ);
            this.Controls.Add(this.KORJENOVANJE);
            this.Controls.Add(this.LOGARITAM);
            this.Controls.Add(this.COSINUS);
            this.Controls.Add(this.SINUS);
            this.Controls.Add(this.DIJELJENJE);
            this.Controls.Add(this.MNOZENJE);
            this.Controls.Add(this.ODUZIMANJE);
            this.Controls.Add(this.ZBROJ);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.UnosOperanda2);
            this.Controls.Add(this.UnosOperanda1);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox UnosOperanda1;
        private System.Windows.Forms.TextBox UnosOperanda2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button ZBROJ;
        private System.Windows.Forms.Button ODUZIMANJE;
        private System.Windows.Forms.Button MNOZENJE;
        private System.Windows.Forms.Button DIJELJENJE;
        private System.Windows.Forms.Button SINUS;
        private System.Windows.Forms.Button COSINUS;
        private System.Windows.Forms.Button LOGARITAM;
        private System.Windows.Forms.Button KORJENOVANJE;
        private System.Windows.Forms.Button KVADRIRAJ;
        private System.Windows.Forms.Label REZULTATI;
        private System.Windows.Forms.TextBox rezZBRAJANJA;
        private System.Windows.Forms.TextBox rezODUZIMANJA;
        private System.Windows.Forms.TextBox rezMNOZENJA;
        private System.Windows.Forms.TextBox rezDIJELJENJA;
        private System.Windows.Forms.TextBox rezSINUSA;
        private System.Windows.Forms.TextBox rezCOSINUSA;
        private System.Windows.Forms.TextBox rezLOGARITMIRANJA;
        private System.Windows.Forms.TextBox rezKORJENOVANJA;
        private System.Windows.Forms.TextBox rezKVADRIRANJA;
        private System.Windows.Forms.Button quit;
    }
}

